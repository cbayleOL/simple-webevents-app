"""Flask config."""
import os
from os import environ, path
# from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
# load_dotenv(path.join(basedir, '.env'))


class Config:
    """Base config."""
    SECRET_KEY = environ.get('SECRET_KEY') or "heel-veel-gehijm"
    SESSION_COOKIE_NAME = environ.get('SESSION_COOKIE_NAME') or 'session'
    STATIC_FOLDER = 'static'
    TEMPLATES_FOLDER = 'templates'


class ProdConfig(Config):
    dbuser = "pietp1raAt"
    dbpasswd = "Smurfi55"
    # dbhost = '192.168.178.11'     # kantoor
    dbhost = '192.168.1.38'       # thuis
    dbname = 'simple_webevents'
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False
    PROD_DATABASE_URI = 'mysql+pymysql://' + dbuser + ':' + dbpasswd + '@' + dbhost + "/" + dbname
    DATABASE_URI = environ.get('PROD_DATABASE_URI')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + dbuser + ':' + dbpasswd + '@' + dbhost + "/" + dbname

class TestConfig(Config):
    # variables
    dbuser = "pietp1raAt"
    dbpasswd = "Smurfi55"
    # dbhost = '192.168.178.11'     # kantoor
    dbhost = '192.168.1.38'       # thuis
    dbname = 'simple_webevents'
    FLASK_ENV = 'testing'
    DEBUG = True
    TESTING = True
    TEST_DATABASE_URI = 'mysql+pymysql://' + dbuser + ':' + dbpasswd + '@' + dbhost + "/" + dbname
    DATABASE_URI = environ.get('TEST_DATABASE_URI')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + dbuser + ':' + dbpasswd + '@' + dbhost + "/" + dbname

class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    TESTING = True
    DATABASE_URI = environ.get('DEV_DATABASE_URI')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'simple_webevents.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
